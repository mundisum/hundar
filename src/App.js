import React, { Component } from "react";
import smallDog from "./small.png";
import mediumDog from "./medium.png";
import largeDog from "./large.png";
import "./App.css";
import NumericInput from "react-numeric-input";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: "m",
      result: "15",
      age: 1,
    };
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.handleNumericChange = this.handleNumericChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleOptionChange = (changeEvent) => {
    this.setState(
      {
        selectedOption: changeEvent.target.value,
      },
      this.handleFormSubmit
    );
  };

  handleNumericChange = (changeEvent) => {
    this.setState(
      {
        age: changeEvent,
      },
      this.handleFormSubmit
    );
  };

  handleFormSubmit = (formSubmitEvent) => {
    this.setState({
      result: this.getHumanAge(this.state.age, this.state.selectedOption),
    });
  };

  getHumanAge = (dogAge, size) => {
    let s = [15, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80];
    let m = [15, 24, 28, 32, 36, 42, 47, 51, 56, 60, 65, 69, 74, 78, 83, 87];
    let l = [
      12,
      22,
      31,
      38,
      45,
      49,
      56,
      64,
      71,
      79,
      86,
      93,
      100,
      107,
      114,
      121,
    ];

    let selection = dogAge - 1;
    selection > 15 ? (selection = 15) : selection;
    switch (size) {
      case "s":
        return s[selection];
      case "m":
        return m[selection];
      case "l":
        return l[selection];
      default:
        return m[selection];
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <form onSubmit={this.handleFormSubmit} name="submitForm">
              <fieldset>
                <legend className="Input-labels">Hur gammal är hunden?</legend>
                <NumericInput
                  min={1}
                  max={25}
                  value={this.state.age}
                  onChange={this.handleNumericChange}
                />
              </fieldset>
              <fieldset>
                <legend className="Input-labels">Storlek</legend>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginTop: "10px",
                  }}
                >
                  <div>
                    <label>
                      <input
                        type="radio"
                        value="s"
                        onChange={this.handleOptionChange}
                        checked={this.state.selectedOption === "s"}
                      />
                      <img src={smallDog}></img>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                        type="radio"
                        value="m"
                        onChange={this.handleOptionChange}
                        checked={this.state.selectedOption === "m"}
                      />
                      <img src={mediumDog}></img>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                        type="radio"
                        value="l"
                        onChange={this.handleOptionChange}
                        checked={this.state.selectedOption === "l"}
                      />
                      <img src={largeDog}></img>
                    </label>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>

          <label className="Result" id="result">
            Din hund är {<strong>{this.state.result}</strong>} människoår
          </label>
        </header>
      </div>
    );
  }
}

export default App;
